package org.example.dao;

import org.example.entities.EntityPerson;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brevleq
 * Date: 10/06/13
 * Time: 15:22
 * To change this template use File | Settings | File Templates.
 */
public class PersonDao extends RetrieverDao<EntityPerson>{

    public static final int NAME_FILTER=1;

    protected PersonDao(SessionFactory sessionFactory) {
        super(sessionFactory, EntityPerson.class);
    }

    @Override
    public List<EntityPerson> retrieveByFilter(Object value, int filter) {
        switch (filter){
            case NAME_FILTER:return retrieveByName((String)value);
            default:return new ArrayList<EntityPerson>();
        }
    }

    private List<EntityPerson> retrieveByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("select e from EntityPerson e where e.completeName like :name order by e.completeName");
        query.setParameter("name", createSearchParameter(name));
        limitTuplesQuantity(query);
        return query.list();
    }
}
