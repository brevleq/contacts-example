package org.example.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brevleq
 * Date: 17/12/12
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public interface IRetrieverDao<T> {

    List<T> retrieveAll();

    List<T> retrieveAll(int firstIndex, int quantity);

    T retrieveById(Serializable id);

    List<T> retrieveByFilter(Object value, int filter);

    List<T> retrieveByFilter(Object value, int filter, int firstIndex, int quantity);

    int getTuplesQuantity();

    T getManagedEntity();

    String getErrorMessage();
}
